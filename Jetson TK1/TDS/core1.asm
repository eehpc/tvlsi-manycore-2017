movi R0, 100                ;move 100 to Reg 0 (output index)
movi R1, 0                  ;move 0 to Reg 1    
movi R2, 16                 ;move 16 to Reg 2
movi R10, 0                 ;move 0 to Reg 10
for 13, 11
mov R14, $[R1]              ;move content at memory $[R1]=$[0] to Reg 14
mov R15, $[R2]              ;move content at memory $[R2]=$[16] to Reg 15
inc R1, R1                  ;increment R1 [initially 0]
mul $[R0], R14, R15         ;multiply [Reg 14 * Reg 15] and store result at memory $[R0]= $[32]
add R10, R10, $[R0]         ;add o/p of previous mul to find final o/p
inc R2, R2                  ;increment R2 [initially 16]
nop
movi R3, 0                  ;move 0 to R3 to check if the no is -ve
bl 15, R10, R3              ;check if output is -ve
bg 16, R10, R3              ;if output is not -ve skip the incrment (in this case 0 is sent to next step)
inc R3, R3                  ;if -ve increment R3
nop
movi R1, 32                 ;move 32 to Reg 1    
movi R2, 48                 ;move 48 to Reg 2
movi R11, 0                 ;move 0 to Reg 11
for 13, 27
mov R14, $[R1]              ;move content at memory $[R1]=$[32] to Reg 14
mov R15, $[R2]              ;move content at memory $[R2]=$[48] to Reg 15
inc R1, R1                  ;increment R1 [initially 32]
mul $[R0], R14, R15         ;multiply [Reg 14 * Reg 15] and store result at memory $[R0]= $[100]
add R11, R11, $[R0]         ;add o/p of previous mul to find final o/p
inc R2, R2                  ;increment R2 [initially 48]
nop
movi R4, 0                  ;move 0 to R4 to check if the no is -ve
bl 31, R11, R4              ;check if output is -ve
bg 32, R11, R4              ;if output is not -ve skip the incrment (in this case 0 is sent to next step)
inc R4, R4                  ;if -ve increment R4
nop
movi R1, 64                 ;move 64 to Reg 1    
movi R2, 80                 ;move 80 to Reg 2
movi R12, 0                 ;move 0 to Reg 12
for 13, 43
mov R14, $[R1]              ;move content at memory $[R1]=$[64] to Reg 14
mov R15, $[R2]              ;move content at memory $[R2]=$[80] to Reg 15
inc R1, R1                  ;increment R1 [initially 64]
mul $[R0], R14, R15         ;multiply [Reg 14 * Reg 15] and store result at memory $[R0]= $[100]
add R12, R12, $[R0]         ;add o/p of previous mul to find final o/p
inc R2, R2                  ;increment R2 [initially 80]
nop
movi R5, 0                  ;move 0 to R5 to check if the no is -ve
bl 47, R12, R5              ;check if output is -ve
bg 48, R12, R5              ;if output is not -ve skip the incrment (in this case 0 is sent to next step)
inc R5, R5                  ;if -ve increment R5
add R4, R4, R3              ;add to check if results are -ve (if both are -ve it will return 2)
nop
add R5, R5, R4              ;R5 is total output for -ve value
bg 53, R12, R11             ;comapre if output from previous core is greater of this core is greater, store result R12
bl 56, R12, R11
movi R1, 5                 
mov R2, R12  
jmp 58
movi R1, 4
mov R2, R11  
nop
bg 61, R2, R10             ;comapre if output from previous core is greater of this core is greater, store result R12
bl 62, R2, R10
jmp 64                 
movi R1, 3
mov R2, R10
in 0                        ;R6 for index
in 0                        ;R7 for max
in 0                        ;R8 for -ve
add R5, R5, R8              ;fial -ve value
movi R9, 6
beq 71, R5, R9
bne 73, R5, R9
movi R1, 7
jmp 76
bg 76, R2, R7
bl 75, R2, R7
mov R1, R6
mov R2, R1
end