import time
import numpy as np
import pycuda.autoinit
from pycuda.compiler import SourceModule
import pycuda.driver as drv

a = np.random.randn(80000).astype(np.float32)
test_vector = np.array([1,2,3,4]).astype(np.float32)
b = np.zeros((1,), dtype=np.float32)

temp_array = np.zeros((16000,), dtype=np.float32)

print "Matrix"
print a
mod = SourceModule("""
__global__ void stress(float *b, float *a, float *test_vector, float *temp_array){

    #include <float.h>
    #include <math.h>
    #define threads 16000
    int idx = blockDim.x * threadIdx.y + threadIdx.x;
    int row_count=threads;
    float sum = 0.0;
    float sum_label = 0.0;
	for (int i= 0; i < 4; i++){
		sum += pow((a[idx + row_count * i] - test_vector[i]),2);
	}

	temp_array[idx] = pow(sum, float(0.5));

	__syncthreads();

	if(idx < threads/2){                                    //8000 values
    		if(temp_array[idx] > temp_array[idx+threads/2]){
        		temp_array[idx] = temp_array[idx+threads/2];
    		}
	} // if ends

	__syncthreads();

	if(idx < threads/4){                                    //4000 values
    		if(temp_array[idx] > temp_array[idx+threads/4]){
        		temp_array[idx] = temp_array[idx+threads/4];
    		}
	} // if ends

	__syncthreads();

	if(idx < threads/8){                                    //2000 values
    		if(temp_array[idx] > temp_array[idx+threads/8]){
        		temp_array[idx] = temp_array[idx+threads/8];
    		}
	} // if ends

	__syncthreads();

	if(idx < threads/16){                                    //1000 values
	    	if(temp_array[idx] > temp_array[idx+threads/16]){
        		temp_array[idx] = temp_array[idx+threads/16];
    		}
	} // if ends

	__syncthreads();

	if(idx < threads/32){                                    //500 values
		if(temp_array[idx] > temp_array[idx+threads/32]){
		        temp_array[idx] = temp_array[idx+threads/32];
    		}
	} // if ends

	__syncthreads();

	if(idx < threads/64){                                    //250 values
    		if(temp_array[idx] > temp_array[idx+threads/64]){
        		temp_array[idx] = temp_array[idx+threads/64];
    		}
	} // if ends

	__syncthreads();

	if(idx < threads/128){                                    //125 values
		    if(temp_array[idx] > temp_array[idx+threads/128]){
		        temp_array[idx] = temp_array[idx+threads/128];
		    }
	} // if ends

	__syncthreads();
/*
	if(idx < threads/256){                                    //125 values
		    if(temp_array[idx] > temp_array[idx+threads/256]){
		        temp_array[idx] = temp_array[idx+threads/256];
		    }

	} // if ends

	__syncthreads();
*/
	if(idx < 60){                                    //60 values-120 to 124 left
		    if(temp_array[idx] > temp_array[idx+60]){
		        temp_array[idx] = temp_array[idx+60];
		    }

	} // if ends

	__syncthreads();

	if(idx < 30){                                    //30 values- 120 to 124 left
		    if(temp_array[idx] > temp_array[idx+30]){
		        temp_array[idx] = temp_array[idx+30];
		    }

	} // if ends

	__syncthreads();

	if(idx < 15){                                    //15 values- 120 to 124 left
		    if(temp_array[idx] > temp_array[idx+15]){
		        temp_array[idx] = temp_array[idx+15];
		    }

	} // if ends

	__syncthreads();

	if(idx < 7){                                    //7 values (15 left)
		    if(temp_array[idx] > temp_array[idx+7]){
		        temp_array[idx] = temp_array[idx+7];
		    }

	} // if ends

	__syncthreads();

	if(idx < 3){                                    //3 values (7 left)
		    if(temp_array[idx] > temp_array[idx+3]){
		        temp_array[idx] = temp_array[idx+3];
		    }

	} // if ends

	__syncthreads();

	if(idx > 119){                                    //2 values at idx =120,121 (124 left)
		if(idx <124){
		    if(temp_array[idx] > temp_array[idx+2]){
		        temp_array[idx] = temp_array[idx+2];
		    }
		}
	} // if ends

	__syncthreads();

	if(idx == 0){

	if(temp_array[0] > temp_array[15]){
  		temp_array[0] = temp_array[15];
	}
	if(temp_array[1] > temp_array[120]){
  		temp_array[1] = temp_array[120];
	}
	if(temp_array[2] > temp_array[121]){
  		temp_array[2] = temp_array[121];
	}
	if(temp_array[7] > temp_array[124]){
  		temp_array[7] = temp_array[124];
	}
	if(temp_array[0] > temp_array[2]){
  		temp_array[0] = temp_array[2];
	}
	if(temp_array[1] > temp_array[7]){
  		temp_array[1] = temp_array[7];
	}
	if(temp_array[2] > temp_array[7]){
  		temp_array[2] = temp_array[7];
	}

    //Sum closest labels
    for(int i = 0; i < 3; ++i){
        sum_label += temp_array[i];
    }
    b[0] = (sum_label > 1.5) ? 1 : 0;
}		

  __syncthreads();


}
""")

start_complete = time.time()
func = mod.get_function("stress")
func(drv.Out(b), drv.In(a), drv.In(test_vector), drv.In(temp_array), block=(1000,1,1), grid=(16,1,1))

stop_complete = time.time()
print "Output"
print b

print stop_complete - start_complete

