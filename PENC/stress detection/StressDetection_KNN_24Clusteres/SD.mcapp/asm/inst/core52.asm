movi R9  , 32767 // fill the R11 with the Max value  
movi R10 , 32767 // fill the R11 with the Max value  
movi R11 , 32767 // fill the R11 with the Max value
movi R12,  0     //fill the R12 with the first initial lable R9
movi R13,  0	 //fill the R13 with the second initial lable R10
movi R14,  0     //fill the R14 with the third initial lable  R11
movi  $0 ,20   // the test data , feature 1 
movi  $1 ,30   // the test data , feature 2
movi  $2 ,20   // the test data , feature 3 
movi  $3 ,20   // the test data , feature 4 
movi  R0 , 0   // the Address
movi  R1 , 0  
for  2 , 49 // each processing core shold process the 200 training samples // each training sample contains 4 features (1 featire from HR and 3 features from accelerometer)
for  100, 49
movi R1, 0  
movi R5, 0 
for  4,28 // calculate the euxlidiean distance between _test sample with each training sample  
inc R0, R0   // sending the read request to the shared memory 
nop          // read the training data's feature from the shared memory , we assume the read data will reserver in R3 register
nop
nop
nop
nop
nop
sub  R3 ,$[R1], R2        // subtract the test feature from the training feature
mul  R4 ,R3   ,R3         // power 2 to calculate the distance 
add  R5 ,R5   ,R4         // adding up all distances 
inc  R1 ,R1               // Next feture 
nop                       // read the label of this training data  and put it in R5
nop
nop
nop
nop
nop
bg   40 , R5,    R9    // sorting 
mov  R11   , R10    
mov  R10   , R9    
mov  R9    , R5  
mov  R12,R2
jmp  47
bg   45  , R5,    R10
mov  R11  , R10   
mov  R10  , R5
mov  R13  , R2    // read the label from the shared memory 
jmp  47   
bg   47   , R5, R11
mov  R11  , R5  
mov  R14  , R2 
nop
movi R0,0
mov $[R0],R9  // put the min distance in the Dmem
inc R0,R0
mov $[R0],R10
inc R0,R0
mov $[R0],R11
inc R0,R0
mov $[R0],R12  // Put the label of nearest neighbors in the Dmem
inc R0,R0
mov $[R0],R13
inc R0,R0
mov $[R0],R14
inc R0    , R0
out  $0   , $0,  54// send the nearest neighbors and their labels to core2
out  $1   , $1,  54
out  $2   , $2,  54
out  $9   , $3,  54 
out  $10  , $4,  54 
out  $11  , $5,  54 
end  