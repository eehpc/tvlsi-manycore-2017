movi R9  , 32767 // fill the R11 with the Max value  
movi R10 , 32767 // fill the R11 with the Max value  
movi R11 , 32767 // fill the R11 with the Max value
movi R12,  0     //fill the R12 with the first initial lable R9
movi R13,  0	 //fill the R13 with the second initial lable R10
movi R14,  0     //fill the R14 with the third initial lable  R11
movi  $0 ,20   // the test data , feature 1 
movi  $1 ,30   // the test data , feature 2
movi  $2 ,40   // the test data , feature 3 
movi  $3 ,50   // the test data , feature 4 
movi  $4 ,60   // the test data , feature 5 
movi  $5 ,70   // the test data , feature 6 
movi  R0 , 0   // the Address
movi  R1 , 6   // first address of the training data in Memory  
for  6 , 41 // each processing core shold process the _92 of the training data
movi R0,0    // point the first test data 
movi R5, 0 
for  12,22 // calculate the euxlidiean distance between _test sample with each training sample  
sub  R3 ,$[R0], $[R1]        // subtract the test feature from the training feature
mul  R4 ,R3   ,R3         // power 2 to calculate the distance 
add  R5 ,R5   ,R4         // adding up all distances 
inc  R1 ,R1               // Next training feture
inc  R0, R0               // Next testing feature    
mov  R2, $[R1]            // The label of the training data 
bg   32, R5, R9    // sorting 
mov  R11   , R10    
mov  R10   , R9    
mov  R9    , R5 
mov  R14   , R13
mov  R13   , R12
mov  R12   , R2       // The label of the training data  
jmp  41
bg    38, R5,R10
mov  R11  , R10   
mov  R10  , R5
mov  R14, R13     // changing the labels
mov  R13  , R2    // read the label from the shared memory 
jmp  41   
bg   41, R5, R11
mov  R11  , R5  
mov  R14  , R2 
nop
nop
in  0   //read the nearest distance from the core 0 
in  0   //read the nearest distance from the core 0
in  0   //read the nearest distance from the core 0
in  0   //read the nearest distance from the core 0
in  0   //read the nearest distance from the core 0
in  0   //read the nearest distance from the core 0 
mov $0  , R0
mov $1  , R1
mov $2  , R2
mov $9  , R3
mov $10  , R4
mov $11  , R5
in  1   //read the nearest distance from the core 1 
in  1   //read the nearest distance from the core 1
in  1   //read the nearest distance from the core 1
in  1   //read the nearest distance from the core 1
in  1   //read the nearest distance from the core 1
in  1   //read the nearest distance from the core 1
mov $3  , R0
mov $4  , R1
mov $5  , R2
mov $12  , R3
mov $13  , R4
mov $14  , R5
in  2  //read the nearest distance from the core 1 
in  2  //read the nearest distance from the core 1
in  2  //read the nearest distance from the core 1
in  2  //read the nearest distance from the core 1
in  2  //read the nearest distance from the core 1
in  2  //read the nearest distance from the core 1
mov $6  , R0
mov $7  , R1
mov $8  , R2
mov $15  , R3
mov $16  , R4
mov $17  , R5
movi R3 ,0
movi R1 ,9  // points the location of the first label
for  9 ,101// for loop for sorting to find the nearest neighbors
bg 90 , $[R3], R9
mov R11, R10
mov R10, R9 
mov R9, $[R3]
mov R14, R13
mov R13, R12
mov R12,$[R1]  // the label
jmp 101
bg 96, $[R3], R10
mov R11, R10
mov R10 ,$[R3]
mov R14, R13
mov R13, $[R1] 
jmp 101
bg 101 ,$[R3] ,R11 
mov R11, $[R3]
mov R13, $[R1]
inc R3, R3
inc R1 , R1 
nop
out  $0,    R9   , 0
out  $1 ,   R10  , 0
out  $2  ,  R11  , 0
out  $3,    R12  , 0
out  $4 ,   R13  , 0
out  $5  ,  R14  , 0
end