movi R9  , 32767 // fill the R11 with the Max value  
movi R10 , 32767 // fill the R11 with the Max value  
movi R11 , 32767 // fill the R11 with the Max value
movi R12,  0     //fill the R12 with the first initial lable R9
movi R13,  0	 //fill the R13 with the second initial lable R10
movi R14,  0     //fill the R14 with the third initial lable  R11
movi  $0 ,20   // the test data , feature 1 
movi  $1 ,30   // the test data , feature 2
movi  $2 ,40   // the test data , feature 3 
movi  $3 ,50   // the test data , feature 4 
movi  $4 ,60   // the test data , feature 5 
movi  $5 ,70   // the test data , feature 6 
movi  R0 , 0   // the Address
movi  R1 , 6   // first address of the training data in Memory  
for  20 , 42 // each processing core shold process the _92 of the training data
movi R0,0    // point the first test data 
movi R5, 0 
for  6,23 // calculate the euxlidiean distance between _test sample with each training sample  
//mov R2,$[R1]  % the training feature  
sub  R3 ,$[R0], $[R1]          // subtract the test feature from the training feature
mul  R4 ,R3   ,R3         // power 2 to calculate the distance 
add  R5 ,R5   ,R4         // adding up all distances 
inc  R1 ,R1               // Next training feture
inc  R0, R0               // Next testing feature    
mov  R2, $[R1]            // The label of the training data 
bg   33, R5,    R9    // sorting 
mov  R11   , R10    
mov  R10   , R9    
mov  R9    , R5 
mov  R14   , R13
mov  R13  , R12
mov  R12   , R2       // The label of the training data  
jmp  42
bg    39 , R5,    R10
mov  R11  , R10   
mov  R10  , R5
mov  R14, R13     // changing the labels
mov  R13  , R2    // read the label from the shared memory 
jmp  42   
bg   42, R5, R11
mov  R11  , R5  
mov  R14  , R2 
nop
movi R0,0
mov $[R0],R9     // The first nearest neighbors 
inc R0,R0
mov $[R0],R10    // The second nearest neighbors 
inc R0,R0
mov $[R0],R11    // The third nearest neighbors 
inc R0,R0  
mov $[R0],R12    // The label for first  nearest neighbors  
inc R0,R0
mov $[R0],R13    // The label for second nearest neighbors 
inc R0,R0
mov $[R0],R14    // The label for third nearest neighbors 
inc R0  ,R0
//  R0   , $0, 3
//out  R1   , $1, 3
//out  R2   , $2, 3
//out  R3   , $3, 3
//out  R4   , $4, 3
//out  R5   , $5, 3
end  