movi R9  , 32767 // fill the R11 with the Max value  
movi R10 , 32767 // fill the R11 with the Max value  
movi R11 , 32767 // fill the R11 with the Max value
movi R12,  0     //fill the R12 with the first initial lable R9
movi R13,  0	 //fill the R13 with the second initial lable R10
movi R14,  0     //fill the R14 with the third initial lable  R11
movi  $0 ,20   // the test data , feature 1 
movi  $1 ,30   // the test data , feature 2
movi  $2 ,40   // the test data , feature 3 
movi  $3 ,50   // the test data , feature 4 
movi  R0 , 0   // the Address
movi  R1 , 0   // first address of the training data in Memory  
for  2 , 48 // each processing core shold process the _92 of the training data
for  100, 47 // calculate the euxlidiean distance between _test sample with each training sample  
movi R1,0    // point the first test data 
movi R5, 0 
for  4,  28
inc R0 , R0
nop          // read the training data's feature from the shared memory , we assume the read data will reserver in R3 register
nop
nop
nop
nop
nop
sub  R3 ,$[R1], R2        // subtract the test feature from the training feature
mul  R4 ,R3   ,R3         // power 2 to calculate the distance 
add  R5 ,R5   ,R4         // adding up all distances 
inc  R1 ,R1               // Next feture 
nop                       // read the label of this training data  and put it in R5
nop
nop
nop
nop
nop
bg   39 , R5,    R9    // sorting 
mov  R11   , R10    
mov  R10   , R9    
mov  R9    , R5  
jmp  47
bg   44  , R5,    R10
mov  R11  , R10   
mov  R10  , R5
mov  R13  , R2    // read the label from the shared memory 
jmp  47  
bg   47   , R5, R11
mov  R11  , R5  
mov  R14  , R2 
nop   
in  34//read the nearest distance from the core 1 
in  34 //read the nearest distance from the core 1
in  34 //read the nearest distance from the core 1
in  34 //read the nearest distance from the core 1
in  34 //read the nearest distance from the core 1
in  34 //read the nearest distance from the core 1
in  36  //read the nearest distance from the core 0 
in  36  //read the nearest distance from the core 0
in  36  //read the nearest distance from the core 0
in  36  //read the nearest label from the core 0
in  36  //read the nearest label from the core 0
in  36  //read the nearest label from the core 0 
in  37//read the nearest distance from the core 1 
in  37//read the nearest distance from the core 1
in  37//read the nearest distance from the core 1
in  37//read the nearest label from the core 1
in  37//read the nearest label from the core 1
in  37//read the nearest label from the core 1
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
nop
movi R3 ,0  // points the location of the first nearest neighbor
movi R1 ,9  // points the location of the first label
for  9 ,test// for loop for sorting to find the nearest neighbors
bg test2 , $[R3], R9    //96 , $[R3], R9
mov R11, R10
mov R10, R9 
mov R9, $[R3]
mov R14, R13
mov R13, R12
mov R12,$[R1]  // the label
jmp test
test2:bg test1, $[R3], R10
mov R11, R10
mov R10 ,$[R3]
mov R14, R13
mov R13, $[R1] 
jmp 107
test1:bg test ,$[R3] ,R11 
mov R11, $[R3]
mov R13, $[R1]
inc R3, R3
inc R1 , R1 
test:nop
out  $6,    R9    , 42
out  $7 ,   R10   , 42
out  $15,    R12  , 42
out  $8  ,  R11   , 42
out  $16 ,   R13  , 42
out  $17  ,  R14  , 42
end