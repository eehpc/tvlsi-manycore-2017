`timescale 1ns / 1ps
// Ali Jafari

module MulLogReg 
#(
	parameter 	      [8:0] 		NUM_FEATURES = 6,
	parameter 	      [8:0] 		NUM_LABELS = 7,
    parameter                       NUM_LRCOEFF = 42,
	parameter 						BR = 16,
	parameter 						BR_coeff = 20,
	parameter   signed 	[2*BR-1:0] 	Z = 0
)
(	input							rst,
	input							clk,
	input							start_load,
	input 		signed   [BR-1:0]   in_test,
	output reg 	 		 [2:0]		label_out,
	output reg 						label_ready,
	output reg                      busy,
	output reg                      load
);

// parameterization
   
 //* Internal Signals *
   reg 					[7:0] 					 coeff_adr;
   reg 			signed 	[2*BR+NUM_FEATURES-1:0]  value;
   reg 			signed 	[BR-1:0]				 in_test_reg [0:NUM_FEATURES-1];
   wire 		signed 	[BR_coeff-1:0] 			 coeff_out;
   wire 		signed 	[2*BR+NUM_FEATURES-1:0]	 dot_prod;
   reg                  [2:0]                    label_temp;	
   reg                  [2:0]                    cnt_labels;
   reg                  [3:0]                    cnt_features;
   reg                  [3:0]                    cnt_load;	
   reg          signed  [2*BR+NUM_FEATURES-1:0]  value_temp;
//* state machine parameters *
	reg 		    [2:0]   state;
	parameter 				INIT_STATE 		= 		0; 
	parameter 				LOAD_STATE 		= 		1; 
	parameter				PROCESS_STATE1  = 		2; 
	parameter				PROCESS_STATE2  = 		3; 
	parameter				DONE_STATE 		= 		4;

//	* Module Instantiations *
//   ROM to save LR coefficients
	 single_port_memory single_port_memory_inst
(
	.clk(clk) ,	// input  clk_sig
	.addr(coeff_adr) ,	// input [MEM_LEN-1:0] addr_sig
	.dout(coeff_out) 	// output [BR-1:0] dout_sig
);	
	
	assign dot_prod   = coeff_out*in_test_reg[cnt_features];

	//	* State Machine *	
	always @ (posedge clk) begin
	
		if(rst == 1'b1) begin	
			coeff_adr 		<= 8'b0;
			value 			<= 0;
			label_ready		<= 0;	
			cnt_labels      <= 3'b001;	
			label_temp      <= 0;
			state           <= INIT_STATE;
			value_temp      <= {{1{1'b1}},{2*BR+NUM_FEATURES-1{1'b0}}};
			cnt_features    <= 0;
			busy            <= 0;
			cnt_load        <= 0; 
			load            <= 1;
			//dot_prod       <= 0;
		end else begin
			case(state)
			   INIT_STATE: begin	
			   label_ready		<= 0;			
			   if (start_load == 1) begin
               state <= LOAD_STATE;
	         end else begin
				   state <= INIT_STATE;
             end				
				end
				LOAD_STATE: begin	
				load           <= 1;			
               if (cnt_load == NUM_FEATURES) begin
			         cnt_load    <= 0;
						state 	   <= PROCESS_STATE1;
						busy        <= 1;
						load        <= 0;
               end else begin
                        in_test_reg[cnt_load] <= in_test;
	                    cnt_load <= cnt_load+1;
						load           <= 1;
						state <= LOAD_STATE;
		         end 				
				end
				
				PROCESS_STATE1: begin			
					if(cnt_labels == NUM_LABELS) begin
						cnt_labels   <= 3'b001;
                        coeff_adr    <= 0;							
						state 	     <= DONE_STATE;
						if (value_temp[2*BR+NUM_FEATURES-1] == 0) begin
						   label_out  <= label_temp;
							state 	  <= DONE_STATE;
						end else begin
						    label_out <= 7;
							state 	  <= DONE_STATE;
						end
					end else begin	
                       value	    <= coeff_out; 
                       coeff_adr    <= coeff_adr + 1;	   				
					   state 	    <= PROCESS_STATE2;
					end					
				end
				
				PROCESS_STATE2: begin		
				  			
					if(cnt_features == NUM_FEATURES) begin
					    value			 <= 0;
						cnt_labels       <= cnt_labels+1;
						cnt_features     <= 0;
						state 	         <= PROCESS_STATE1;
						if (value >= value_temp) begin
						   value_temp <= value;
						   label_temp <= cnt_labels;
							state 	  <= PROCESS_STATE1;
						end else begin
						   value_temp  <= value_temp;
							label_temp <= label_temp;
							state 	   <= PROCESS_STATE1;
						end
					end else begin
					   value		 <= value + dot_prod;	
		               cnt_features  <= cnt_features+1;	
			           coeff_adr     <= coeff_adr + 1;				
					   state 	     <= PROCESS_STATE2;
					end					
				end
				
				DONE_STATE: begin
				    busy           <= 0;
					label_ready    <= 1;	
	                value          <= 0;				
					state 		   <= INIT_STATE;
					load           <= 1;
					value_temp     <= {{1{1'b1}},{2*BR+NUM_FEATURES-1{1'b0}}};
				end	
			endcase		
		end
	end
endmodule
