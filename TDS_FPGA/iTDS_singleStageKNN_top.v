`timescale 1ns / 1ps
// Ali Jafari

module iTDS_singleStageKNN_top(
    input reset,
    input clk,
    input sdo,
    output scl,
    output sda,
    output cs_lf,
    output cs_lb,
    output cs_rf,
    output cs_rb,
    output [2:0] label_out_sig,
    output busy_sig);

parameter EMF_IN_BIT_WIDTH = 16;
parameter EMF_OUT_BIT_WIDTH = 16;
parameter                           BR = 16;
parameter         [8:0]         NUM_FEATURES = 12;
//* Internal Signals *
wire                       start_load_sig;
wire   signed   [BR-1:0]   in_test_sig;
wire                      load_sig;
wire label_ready_sig;
    
// SPI
wire            spi_start, left_active;
wire            spi_ready;
wire   [47:0]   spi_front_data, spi_back_data;
wire            load_emf_data;
assign load_emf_data = (load_sig & ~busy_sig);

//  * Module Instantiations *
// SPI
spi_sm spi_sm_inst
(
    .sys_clk(clk) ,                 // input  sys_clk_sig
    .sys_reset_n(~reset) ,          // input  sys_reset_n_sig
    .start(spi_start) ,             // input  start_sig
    .left_active(left_active) ,     // input  left_active_sig
    .sdo(sdo) ,                     // input  sdo_sig
    .ready(spi_ready) ,             // output  ready_sig
    .front_data(spi_front_data) ,   // output [47:0] front_data_sig
    .back_data(spi_back_data) ,     // output [47:0] back_data_sig
    .scl(scl) ,                     // output  scl_sig
    .sda(sda) ,                     // output  sda_sig
    .cs_lf(cs_lf) ,                 // output  cs_lf_sig
    .cs_lb(cs_lb) ,                 // output  cs_lb_sig
    .cs_rf(cs_rf) ,                 // output  cs_rf_sig
    .cs_rb(cs_rb)                   // output  cs_rb_sig
);

    
EMF #(
    .DATA_IN_PRECISION(EMF_IN_BIT_WIDTH), // number of bits to use for each x/y/z value. Max 16
    .DATA_OUT_PRECISION(EMF_OUT_BIT_WIDTH)
    )EMF_inst
(
    .clk(clk) , // input  clk_sig
    .reset_n(~reset) ,  // input  reset_n_sig
    .i2c_ready(spi_ready) , // input  spi_ready_sig
    .i2c_front_data(spi_front_data) ,   // input [47:0] spi_front_data_sig
    .i2c_back_data(spi_back_data) , // input [47:0] spi_back_data_sig
    .load_data(load_emf_data) , // input  load_data_sig
    .i2c_start(spi_start) , // output  spi_start_sig
    .i2c_left_active(left_active) , // output  i2c_left_active_sig
    .data_out_valid(start_load_sig) ,   // output  data_out_valid_sig
    .data_out(in_test_sig)  // output [DATA_OUT_PRECISION-1:0] data_out_sig
);  // Clssifier LR

MulLogReg MulLogReg_inst
(
    .rst(reset) ,                 // input  rst_sig
    .clk(clk) ,                   // input  clk_sig
    .start_load(start_load_sig) ,     // input  start_load_sig
    .in_test(in_test_sig) ,         // input  [BR-1:0] in_test_sig
    .label_out(label_out_sig) ,   // output [2:0] label_out_sig
    .label_ready(label_ready_sig) , // output  label_ready_sig
    .busy(busy_sig) ,                 // output  busy_sig
    .load(load_sig)                   // output  load_sig
);


endmodule
