// ----------------------------------------------------------------------------
// This reference design and source code is being provided on an "as-is" basis 
// and as an accommodation, and therefore all warranties, representations or 
// guarantees of any kind (whether express, implied or statutory) including, 
// without limitation, warranties of merchantability, non-infringement, or  
// fitness for a particular purpose, are specifically disclaimed.
//
// This source code may only be used in an Altera programmable logic device
// and may not be distributed without permission from Macnica Americas, Inc.  
// It is provided free of royalties or fees of any kind.
// ----------------------------------------------------------------------------
//
// Description  | This design provides simple I2C access to Manica Odyssey IoT
//              | The design is largely the OpenCores I2C Slave.
//              | 	http://opencores.org/project,i2cslave
//              | In general, this slave requires the normal I2C addressing
//              | but additionally a memory offset value to which subsequent
//              | I2C reads/writes occur.  This offset increments every data
//              | transfer until end-of-transaction (stop bit).
//              | 
//              | As provided, this design has the following address space:
//              |    BASE_ADDR		7'h30
//              |    AUTO_ADDR		BASE_ADDR + 0
//              |    LED_ADDR			BASE_ADDR + 1
//              |    SWITCH_ADDR		BASE_ADDR + 2
//              |    BUTTON_ADDR		BASE_ADDR + 3
//              | Note the above is pure addressing.  Actual I2C addressing 
//              | is [7:0,W0/R1] such that:
//              | 	0x60 = address for write access
//              | 	0x61 = address for read access
//              | Sample Write Access to LED register:
//              | 	Start
//              | 	0x60	(address,Write)
//              | 	0x10	(offset for LED register)
//              | 	0xaa	(LED pattern)
//              | 	Stop
//              | Sample Read Access from BUTTON register:
//              | 	Start
//              | 	0x60	(address,Write)
//              | 	0x10	(offset for BUTTON register)
//              | 	Stop
//              | 	Start
//              | 	0x61 (address,Read)
//              | 	<data byte transfer>
//              | 	Stop
//              | 
// Modification | This design can be used to provide I2C access to any design
//              | targeting the Odyssey IoT board.  To do so you should copy the following
//              | files local to your project and modify as indicated.  (Files not requiring
//              | modification should not be copied locally and instead referenced as shown
//              | in the template project).
//              | 	top.v:
//              | 		Instance your custom logic here with 8 bit registers for control/status
//              | 		Connect 8 bit registers to i2cslave instance (modified for desired
//              | 		number of regs)
//              | 	i2cSlave.v:
//              | 		Modify portlist for number of 8bit regs desired
//              | 		Connect ports to register_interface instance
//              | 	register_Interface.v:
//              | 		Modify port list for number of 8bit regs desired
//              | 		Modify I2C_READ and I2C_WRITE case statements
//              | 	i2cSlave_define.v
//              | 		Controls base address and freq of operation.
//              | 		Currently 0x30, 50MHz core clock, 100kHz I2C
//              | 		Copy/Modification is optional if these do not need to be changed
//              | 
// Formatting   | Tabs set at 3 spaces.  Port names 'UPPER' case.
//              | Internal wires and registers are 'lower' case.
//              | Module names 'lower' case to match lower case filename.v
//              | Parameters are first character 'Upper' case.
//              | Active low signals are identified with '_n' or '_N'
//              | appended to the wire, register, or port name.
//              | 
//              | _r suffix on a register name indicates a simple
//              | re-registering of another wire or register.
//              | 
//              | _rr suffix on a register name indicates a second
//              | re-registering of a register.
//              | 
//              | _metastab indicates clock domain crossing
//
// ----------------------------------------------------------------------------

`timescale 1ns/1ps

//--------------------------
//	Declaration and ports
//-----------
module top (
	
	//global
	input					IN_CLOCK,
	
	//spi signals
    input IN_SPI_SDO,
    output OUT_SPI_SCL,
    output OUT_SPI_SDA,
    output OUT_SPI_CS_LF,
    output OUT_SPI_CS_LB,
    output OUT_SPI_CS_RF,
    output OUT_SPI_CS_RB,
    
	//i2c for bluetooth
	input					IN_SCL,
	inout					INOUT_SDA,
	
	//gpio
	output	[7:0]		OUT_LED,
	input		[2:0]		IN_SWITCH,
	input		[1:0]		IN_BUTTON_N
//	output   [2:0]    MY_LEDS_OUT,   //MAX10 WORKSHOP
//	output   [2:0]    led_sig,
//	output            busy_sig
//	output   [2:0]    label_out_sig,
//	output  myclk,
//	input rst
);

//--------------------------
//	Regs and Wires
//-----------
	reg		[7:0]		autoreg;
	reg					reset_n_r;
	reg					reset_n_rr;
	wire					clock;
	wire					locked;
   wire     [7:0]    led_sig; //TDS
	wire     [2:0]    label_out_sig;
	
assign    OUT_LED = {5'b11111,~label_out_sig[2:0]};

//--------------------------
//	The I2C Slave
// -----------
i2cSlave i2c_inst(
	.clk(clock),
	.rst(!reset_n_rr),
	.sda(INOUT_SDA),
	.scl(IN_SCL),
	.reg0_auto_in(autoreg[7:0]),
	.reg1_led_out(),//OUT_LED[7:0]),
	.reg2_switch_in({5'b0,IN_SWITCH[2:0]}),
	.reg3_button_in({6'b0,IN_BUTTON_N[1:0]}),
	.led_sig_tds(label_out_sig)
);

	// Instantiation of the I2C+KNN
iTDS_singleStageKNN_top iTDS_singleStageKNN_top_inst
(
	.reset(!reset_n_rr) ,	// input  reset_sig
	.clk(clock) ,	// input  clk_sig
	.sdo(IN_SPI_SDO) ,	// input  sdo_sig
	.scl(OUT_SPI_SCL) ,	// output  scl_sig
	.sda(OUT_SPI_SDA) ,	// output  sda_sig
	.cs_lf(OUT_SPI_CS_LF) ,	// output  cs_lf_sig
	.cs_lb(OUT_SPI_CS_LB) ,	// output  cs_lb_sig
	.cs_rf(OUT_SPI_CS_RF) ,	// output  cs_rf_sig
	.cs_rb(OUT_SPI_CS_RB) ,	// output  cs_rb_sig
	.label_out_sig(label_out_sig) ,	// output [2:0] label_out_sig_sig
	.busy_sig() 	// output  busy_sig_sig
);
//--------------------------
//	PLL
//-----------
pll pll_inst (
	.inclk0 ( IN_CLOCK ),
	.c0 ( clock ),
	.locked ( locked )
	);


//--------------------------
//	Reset - Synchronous deassert after PLL locked
//-----------
//Synchronize Reset
always @(posedge IN_CLOCK)
	begin
	if (!locked)
		begin
		reset_n_r <= 1'b0;
		reset_n_rr <= 1'b0;
		end
	else
		begin
		reset_n_r <= 1'b1;
		reset_n_rr <= reset_n_r;
		end
	end


//--------------------------
//	Encode the auto register
// The auto reg is driven by HW and polled by software for indicator control
//	indicator1 = button 1 active, autoreg --> 0x55
//	indicator2 = button 2 active, autoreg --> 0xaa
//-----------
always @(posedge clock)
	begin
	if (!reset_n_rr)
		begin
		autoreg[7:0] <= 8'h00;
		end
	else
		begin
		case (~IN_BUTTON_N[1:0])
			2'b01		: autoreg <= 8'h55;
			2'b10		: autoreg <= 8'haa;
			default	: autoreg <= 8'h00;
		endcase
		end
	end

	endmodule
