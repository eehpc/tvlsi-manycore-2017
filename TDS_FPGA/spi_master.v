// Author: Fred Buswell

module spi_master(
    input clk,                  // input clock
    input reset_n,
    input ena,              // start signal. latch command.
    input [5:0] reg_addr,   // slave register address.
    input rw,           // '0' is write, '1' is read
    input [7:0] data_wr, // data to write to slave register
    input [1:0] sensor_sel, // selects which sensor to access
    output reg busy,
    output [7:0] data_rd,
    // device interface
    output cs_0, // CS. chip select for sensor 0 = Left Front.
    output cs_1, // CS. chip select for sensor 1 = Left Back.
    output cs_2, // CS. chip select for sensor 2 = Right Front.
    output cs_3, // CS. chip select for sensor 3 = Right Back.
    output reg spc, // SCL. serial port clock
    output reg sdi, // SDA. slave data in
    input sdo); //SDO. slave data out
    
    parameter input_clk = 50_000_000;
    parameter bus_clk   = 8_000_000;
    localparam clk_divider = input_clk/bus_clk/2;
    localparam clk_divider_1 = clk_divider-1;
    localparam clk_divider2 = clk_divider*2;
    localparam clk_divider2_1 = clk_divider2-1;
    localparam multi_single = 1'b0; // always do single write/reads.
    
    // registers for inputs
    reg [7:0] rw_ms_addr;
    reg [7:0] data_wr_reg;
    reg [3:0] sensor_sel_reg;
    reg [2:0] bit_count;
    reg [7:0] sdo_data;
    
    reg cnt_en;
    integer clk_count; // used to divide the input clock.

    reg [3:0] state;
    parameter [2:0] IDLE = 0,
                    SEND_COMMAND = 1,
                    READ_DATA = 2,
                    WRITE_DATA = 3,
                    FINISH_COUNT = 4,
                    DELAY_READ = 5;
    
    assign cs_0 = sensor_sel_reg[0];
    assign cs_1 = sensor_sel_reg[1];
    assign cs_2 = sensor_sel_reg[2];
    assign cs_3 = sensor_sel_reg[3];
    assign data_rd = sdo_data;
    
    always @ (posedge clk or negedge reset_n)
    begin
        if( reset_n == 1'b0 )
        begin
            // TODO: reset signals
            clk_count <= 0;
            cnt_en <= 1'b0;
            state <= IDLE;
            bit_count <= 3'b111;
            sensor_sel_reg <= 4'b1111;
            spc <= 1'b1;
            sdi <= 1'b0;
            busy <= 1'b0;
            sdo_data <= 8'd0;
        end else
        begin
            if (cnt_en == 1'b1)
            begin
                if (clk_count == clk_divider2_1)
                begin
                    clk_count <= 0;
                end else begin
                    clk_count <= clk_count + 1;
                end
            end else begin
                clk_count <= 0;
            end
            
            case (state)
                IDLE: begin // IDLE state waiting for ena
                    if (ena == 1'b1)
                    begin
                        cnt_en <= 1'b1;
                        rw_ms_addr <= {rw, 1'b0, reg_addr};
                        data_wr_reg <= data_wr;
                        bit_count <= 3'b111;
                        busy <= 1'b1;
                        state <= SEND_COMMAND;
                        case (sensor_sel)
                            2'b00: begin
                                sensor_sel_reg <= 4'b1110;
                            end
                            2'b01: begin
                                sensor_sel_reg <= 4'b1101;
                            end
                            2'b10: begin
                                sensor_sel_reg <= 4'b1011;
                            end
                            2'b11: begin
                                sensor_sel_reg <= 4'b0111;
                            end
                        endcase
                    end else begin
                        cnt_en <= 1'b0;
                        sensor_sel_reg <= 4'b1111;
                        spc <= 1'b1;
                        busy <= 1'b0;
                        state <= IDLE;
                    end
                end
                
                SEND_COMMAND: begin // Transmit the rw, ms, and address bits
                    if (clk_count == clk_divider_1) // SPC falling edge. set tx data.
                    begin
                        spc <= 1'b0;
                        sdi <= rw_ms_addr[bit_count];
                        if (bit_count == 3'b000) // done transmitting rw_ms_addr
                        begin
                            bit_count <= 3'b111; // set for read/write data
                            if (rw_ms_addr[7] == 1'b1) // read operation
                            begin
                                state <= DELAY_READ;
                            end else begin // else write operation
                                state <= WRITE_DATA;
                            end
                        end else begin
                            bit_count <= bit_count - 1'b1;
                        end
                    end else if (clk_count == clk_divider2_1) // SPC rising edge
                    begin
                        spc <= 1'b1;
                    end
                end

                WRITE_DATA: begin // Transmit data bits
                    if (clk_count == clk_divider_1) // SPC falling edge. set tx data.
                    begin
                        spc <= 1'b0;
                        sdi <= data_wr_reg[bit_count];
                        if (bit_count == 3'b000) // done transmitting data_wr_reg
                        begin
                            bit_count <= 3'b111; // set to default
                            state <= FINISH_COUNT;
                        end else begin
                            bit_count <= bit_count - 1'b1;
                        end
                    end else if (clk_count == clk_divider2_1) // SPC rising edge
                    begin
                        spc <= 1'b1;
                    end
                end
                DELAY_READ: begin
                    if (clk_count == clk_divider2_1) // SPC rising edge
                    begin
                        spc <= 1'b1;
                        state <= READ_DATA;
                    end
                end
                READ_DATA: begin // Store incoming data bits
                    if (clk_count == clk_divider_1) // SPC falling edge.
                    begin
                        spc <= 1'b0;
                    end else if (clk_count == clk_divider2_1) // SPC rising edge
                    begin
                        spc <= 1'b1;
                        sdo_data <= {sdo_data[6:0], sdo}; // append new bit to data in.
                        if (bit_count == 3'b000) // done reading
                        begin
                            bit_count <= 3'b111; // set to default
                            state <= FINISH_COUNT;
                        end else begin
                            bit_count <= bit_count - 1'b1;
                        end
                    end
                end

                FINISH_COUNT: begin // delay to finish spc cycle before retuning to IDLE state
                    if (clk_count == clk_divider_1)
                    begin
                        busy <= 1'b0;
                        cnt_en <= 1'b0;
                        sensor_sel_reg <= 4'b1111;
                        state <= IDLE;
                    end else if (clk_count == clk_divider2_1) // SPC rising edge
                    begin
                        spc <= 1'b1;
                    end
                end  
            endcase
        end
    end
endmodule
