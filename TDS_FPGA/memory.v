`timescale 1ns / 1ps
// To be used as a ROM
module single_port_memory(clk, addr, dout);
   
   parameter BR = 20;
	parameter MEM_LEN = 42; //65;
	//parameter MEM_FILE = "C:/Users/Fred/Documents/UMBC/Research/Altera_Projects/TDS_SPI_EMF_LR/MEM/lr_coef.txt";
	
	parameter MEM_FILE = "D:/Dropbox/UMBC_GT_TDS/matlab/coefficients/lr_coef.txt";
	
	input 		         clk;
   input 	  [7:0] 	   addr;     
   output wire [BR-1:0]  dout;			
	
	reg signed [BR-1:0] mem	[0:MEM_LEN-1];//64];
	
   initial begin
      $readmemb(MEM_FILE, mem); // memory_list is memory file
   end
	
	assign dout = mem[addr];
	
endmodule
