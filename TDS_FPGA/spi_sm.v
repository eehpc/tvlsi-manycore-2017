// Author: Fred Buswell

module spi_sm(
    input sys_clk,
    input sys_reset_n,
    input start,
    input left_active,
    input sdo,
    output reg ready,
    output [47:0] front_data,
    output [47:0] back_data,
    output scl,
    output sda,
    output cs_lf,
    output cs_lb,
    output cs_rf,
    output cs_rb);

    wire spi_busy;
    wire [7:0] rdata;
    wire [1:0] sensor_sel;
    

    reg [47:0] data [1:0];
    reg ena, rw;
    reg [7:0] wdata;
    reg count_sensor ;
    reg [2:0] count_data_in;
    reg [3:0] count_data;
    reg [2:0] current_state, next_state;
    reg [5:0] spi_reg_addr;
    
    parameter [2:0] IDLE=               3'b000, 
                    SET_TX_DATA=        3'b001,
                    WAIT_SPI_TX_START_TX = 3'b010,
                    WAIT_SPI_TX_DONE=   3'b011,
                    WAIT_SPI_TX_START_RX=    3'b100,
                    SPI_RX_MAG_DATA=    3'b101;

    assign front_data = data[0];
    assign back_data = data[1];
    assign sensor_sel = {~left_active,count_sensor};
    
    spi_master spi_master_inst(
        // inputs
        .clk(sys_clk),                  // input clock
        .reset_n(sys_reset_n),
        .ena(ena),              // start signal. latch command.
        .reg_addr(spi_reg_addr),   // [5:0] slave register address.
        .rw(rw),           // '0' is write, '1' is read
        .data_wr(wdata), // [7:0] data to write to slave register
        .sensor_sel(sensor_sel), // [1:0] selects which sensor to access
        .sdo(sdo), //SDO. slave data out
        // outputs
        .busy(spi_busy),
        .data_rd(rdata), //[7:0] output. Data from sensor.
        .cs_0(cs_lf), // CS. chip select for sensor 0 = Left Front.
        .cs_1(cs_lb), // CS. chip select for sensor 1 = Left Back.
        .cs_2(cs_rf), // CS. chip select for sensor 2 = Right Front.
        .cs_3(cs_rb), // CS. chip select for sensor 3 = Right Back.
        .spc(scl), // SCL. serial port clock
        .sdi(sda)); // SDA. slave data in
        
        
    always @ ( next_state)
    begin
        current_state <= next_state;
    end

    always @ (posedge sys_clk or negedge sys_reset_n)
    begin
        if( sys_reset_n == 1'b0 )
        begin
            next_state = IDLE;
            count_sensor = 1'd0;
            count_data_in = 3'd0;
            count_data = 4'd0;
            ena = 1'b0;
            ready = 1'b1;
        end else
        begin
            case ( current_state )
                IDLE:
                    begin
                        ready = 1'b1;
                        ena = 1'b0;
                        if( start == 1'b1 )
                        begin
                            ready = 1'b0;
                            next_state = SET_TX_DATA;
                        end
                    end
                SET_TX_DATA:
                    begin
                        if (spi_busy == 1'b0) // SPI ready for write
                        begin
                            ena = 1'b1;
                            next_state = WAIT_SPI_TX_START_TX;
                            case (count_data)
                                4'd0:
                                    begin
                                        rw = 1'b0;
                                        spi_reg_addr = 6'h12; // INT_CTRL_M (01_0010)
                                        wdata = 8'hE0; // 1110_0000: enable interrupts on x, y, and z magnetic data
                                    end
                                4'd1:
                                    begin
                                        rw = 1'b0;
                                        spi_reg_addr = 6'h20; // CTRL1
                                        wdata = 8'h60; // 0110_0000: Acceleration data rate = 100Hz, Acceleration x, y, and z axis disabled.
                                    end
                                4'd2:
                                    begin
                                        rw = 1'b0;
                                        spi_reg_addr = 6'h24; // CTRL5
                                        wdata = 8'h74; // 0111_0100: Temp Sensor =Disabled, Mag. Resolution =HIGH, Mag. data rate =100Hz
                                    end
                                4'd3:
                                    begin
                                        rw = 1'b0;
                                        spi_reg_addr = 6'h25; // CTRL6
                                        wdata = 8'b0010_0000; // 0xx0_0000: xx=Magnetic Full Scale: 11=+- 12 Gauss, 10=+-8,01=+-4,00=+-2
                                    end
                                4'd4:
                                    begin
                                        rw = 1'b0;
                                        spi_reg_addr = 6'h26; // CTRL7
                                        wdata = 8'h00; // 0000_0000: Magnetic Sensor Mode = Continuous-Conversion Mode
                                    end
                                4'd5:
                                    begin
                                        rw = 1'b1;
                                        spi_reg_addr = 6'h08; // OUT_X_L_M
                                        wdata = 8'h00; // Don't care.
                                        next_state = WAIT_SPI_TX_START_RX;
                                    end
                                4'd6:
                                    begin
                                        rw = 1'b1;
                                        spi_reg_addr = 6'h09; // OUT_X_H_M
                                        wdata = 8'h00; // Don't care.
                                        next_state = WAIT_SPI_TX_START_RX;
                                    end
                                4'd7:
                                    begin
                                        rw = 1'b1;
                                        spi_reg_addr = 6'h0A; // OUT_Y_L_M
                                        wdata = 8'h00; // Don't care.
                                        next_state = WAIT_SPI_TX_START_RX;
                                    end
                                4'd8:
                                    begin
                                        rw = 1'b1;
                                        spi_reg_addr = 6'h0B; // OUT_Y_H_M
                                        wdata = 8'h00; // Don't care.
                                        next_state = WAIT_SPI_TX_START_RX;
                                    end
                                4'd9:
                                    begin
                                        rw = 1'b1;
                                        spi_reg_addr = 6'h0C; // OUT_Z_L_M
                                        wdata = 8'h00; // Don't care.
                                        next_state = WAIT_SPI_TX_START_RX;
                                    end
                                4'd10:
                                    begin
                                        rw = 1'b1;
                                        spi_reg_addr = 6'h0D; // OUT_Z_H_M
                                        wdata = 8'h00; // Don't care.
                                        next_state = WAIT_SPI_TX_START_RX;
                                    end
                            endcase
                        end
                    end
                WAIT_SPI_TX_START_TX: // waits for spi to start.
                    begin
                        ena = 1'b0;
                        if (spi_busy == 1'b0)
                        begin
                            next_state = WAIT_SPI_TX_START_TX;
                        end else begin
                            next_state = WAIT_SPI_TX_DONE;
                        end
                    end
                WAIT_SPI_TX_DONE: // waits for spi to finish writing byte then goes to next register.
                    begin
                        ena = 1'b0;
                        if (spi_busy == 1'b1)
                        begin
                            next_state = WAIT_SPI_TX_DONE;
                        end else begin
                            count_data = count_data + 1;
                            next_state = SET_TX_DATA;
                        end
                    end
                WAIT_SPI_TX_START_RX: // waits for spi to start.
                    begin
                        ena = 1'b0;
                        if (spi_busy == 1'b0)
                        begin
                            next_state = WAIT_SPI_TX_START_RX;
                        end else begin
                            next_state = SPI_RX_MAG_DATA;
                        end
                    end
                SPI_RX_MAG_DATA:
                    begin
                        ena = 1'b0;
                        if (spi_busy == 1'b0)
                        begin
                            case (count_data_in)
                                // first 12 bits
                                3'd0:
                                    begin
                                        data[count_sensor] [7:0] = rdata[7:0];
                                        // data[count_sensor+1] [7:0] = right_rdata[7:0];
                                        count_data_in = count_data_in + 1;
                                        count_data = count_data + 1;
                                        next_state = SET_TX_DATA;
                                    end
                                3'd1:
                                    begin
                                        //data[count_sensor] [11:8] = rdata[3:0];
                                        //data[count_sensor+1] [11:8] = right_rdata[3:0];
                                        data[count_sensor] [15:8] = rdata[7:0];
                                        // data[count_sensor+1] [15:8] = right_rdata[7:0];
                                        count_data_in = count_data_in + 1;
                                        count_data = count_data + 1;
                                        next_state = SET_TX_DATA;
                                    end
                                // second 12 bits
                                3'd2:
                                    begin
                                        //data[count_sensor] [19:12] = rdata[7:0];
                                        //data[count_sensor+1] [19:12] = right_rdata[7:0];
                                        data[count_sensor] [23:16] = rdata[7:0];
                                        // data[count_sensor+1] [23:16] = right_rdata[7:0];
                                        count_data_in = count_data_in + 1;
                                        count_data = count_data + 1;
                                        next_state = SET_TX_DATA;
                                    end
                                3'd3:
                                    begin
                                        //data[count_sensor] [23:20] = rdata[3:0];
                                        //data[count_sensor+1] [23:20] = right_rdata[3:0];
                                        data[count_sensor] [31:24] = rdata[7:0];
                                        // data[count_sensor+1] [31:24] = right_rdata[7:0];
                                        count_data_in = count_data_in + 1;
                                        count_data = count_data + 1;
                                        next_state = SET_TX_DATA;
                                    end
                                //third 12 bits
                                3'd4:
                                    begin
                                        //data[count_sensor] [31:24] = rdata[7:0];
                                        //data[count_sensor+1] [31:24] = right_rdata[7:0];
                                        data[count_sensor] [39:32] = rdata[7:0];
                                        // data[count_sensor+1] [39:32] = right_rdata[7:0];
                                        count_data_in = count_data_in + 1;
                                        count_data = count_data + 1;
                                        next_state = SET_TX_DATA;
                                    end
                                3'd5:
                                    begin
                                        //data[count_sensor] [35:32] = rdata[3:0];
                                        //data[count_sensor+1] [35:32] = right_rdata[3:0];
                                        data[count_sensor] [47:40] = rdata[7:0];
                                        // data[count_sensor+1] [47:40] = right_rdata[7:0];
                                        count_data_in = 0;
                                        count_data = 0;
                                        
                                        // done reading 12-bit xyz data from one sensor.
                                        // repeat states 0-5 for other three sensors
                                        if (count_sensor == 1'd1)
                                        begin
                                            count_sensor = 1'd0;
                                            ready = 1'b1;
                                            next_state = IDLE; 
                                        end else begin
                                            count_sensor = 1'd1;
                                            next_state = SET_TX_DATA; 
                                        end
                                    end   
                            endcase
                        end
                    end
            endcase
        end
    end

endmodule
