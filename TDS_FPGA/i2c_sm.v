///////////////////////////////////////////////////////////////////////////////////////////////////
// Company: <Name>
//
// File: i2c_sm.v
// File history:
//      <Revision number>: <Date>: <Comments>
//      <Revision number>: <Date>: <Comments>
//      <Revision number>: <Date>: <Comments>
//
// Description: 
//
// <Description here>
//
// Targeted device: <Family::IGLOO> <Die::AGLN250V2> <Package::100 VQFP>
// Author: <Name>
//
/////////////////////////////////////////////////////////////////////////////////////////////////// 

module i2c_sm(
	input sys_clk,
	input sys_reset,
	input start,
   input left_active,
	output reg ready,
	output [47:0] front_data,
	output [47:0] back_data,
	inout sda_l,
	inout scl_l,
	inout sda_r,
	inout scl_r);

	wire i2c_busy;
	wire [7:0] rdata;
	wire ack_error;

    reg [47:0] data [1:0];
	reg ena, rw;
	reg [6:0] addr;
	reg [7:0] wdata;
	reg count_sensor ;
	reg [2:0] count_data_in, count_data ;
    reg [3:0] current_state, next_state;
	
    parameter [3:0] IDLE=			 	4'd00, 
					I2C_TX_REG_ADDR=		4'd01,
					I2C_TX_REG_DATA= 	4'd02,
					WAIT_I2C_REG_ADDR_DONE= 		4'd03,
					WAIT_BUSY_1= 		4'd04,
					WAIT_I2C_RX_DATA= 		4'd05,
					WAIT_BUSY_2= 		4'd06;

    assign front_data = data[0];
    assign back_data = data[1];

	i2c_master i2c_driver(
		// in
		.clk       (sys_clk),  //IN      STD_LOGIC,                    //system clock
		.reset   (sys_reset),  //IN      STD_LOGIC,                    //active low reset
		.ena       (ena),  //IN      STD_LOGIC,                    //latch in command
		.addr      (addr),  //IN      STD_LOGIC_VECTOR(6 DOWNTO 0), //address of target slave
		.rw        (rw),  //IN      STD_LOGIC,                    //'0' is write, '1' is read
		.data_wr   (wdata),  //IN      STD_LOGIC_VECTOR(7 DOWNTO 0), //data to write to slave
        .left_active (left_active),
		// out
		.busy      (i2c_busy),  //OUT     STD_LOGIC,                    //indicates transaction in progress
		.data_rd   (rdata),  //OUT     STD_LOGIC_VECTOR(7 DOWNTO 0), //data read from slave
		.ack_error (ack_error),  //BUFFER  STD_LOGIC,                    //flag if improper acknowledge from slave
		.sda_l       (sda_l),  //INOUT   STD_LOGIC,                    //serial data output of i2c bus
		.scl_l       (scl_l),  //INOUT   STD_LOGIC);                   //serial clock output of i2c bus			
		.sda_r       (sda_r),  //INOUT   STD_LOGIC,                    //serial data output of i2c bus
		.scl_r       (scl_r));  //INOUT   STD_LOGIC);                   //serial clock output of i2c bus			
		
    always @ ( next_state)
    begin
        current_state <= next_state;
    end

	always @ (posedge sys_clk  or posedge sys_reset)
	begin
		if( sys_reset == 1'b1 )
		begin
			next_state = IDLE;
            count_sensor = 1'd0;
            count_data_in = 3'd0;
            count_data = 3'd0;
            ena = 1'b0;
            ready = 1'b1;
		end else
		begin
            case ( current_state )
                IDLE:
					begin
                        ready = 1'b1;
                        ena = 1'b0;
						if( start == 1'b1 )
						begin
                            ready = 1'b0;
							addr = 7'b00111_01; // 01=front, 10=back
							next_state = I2C_TX_REG_ADDR;
						end
					end
                I2C_TX_REG_ADDR:
                    begin
                        // LED = 8'b1111_1110;
                        if (i2c_busy == 1'b0) // I2C ready for write
                        begin
                            ena = 1'b1;
                            rw = 1'b0; // always write reg address
                            next_state = I2C_TX_REG_DATA;
                            case (count_data)
                                3'd0:
                                    begin
                                        wdata = 8'h12; // INT_CTRL_M ([0=r/w][0=mult/sing]01_0010)
                                    end
                                3'd1:
                                    begin
                                        wdata = 8'h20; // CTRL1
                                    end
                                3'd2:
                                    begin
                                        wdata = 8'h24; // CTRL5
                                    end
                                3'd3:
                                    begin
                                        wdata = 8'h25; // CTRL6
                                    end
                                3'd4:
                                    begin
                                        wdata = 8'h26; // CTRL7
                                    end
                                3'd5:
                                    begin
                                        wdata = 8'h88; // OUT_X_L_M (Read multiple: OUT_X_L_M, OUT_X_H_M, OUT_Y_L_M, etc.)
                                        //next_state = I2C_RX_MAG_DATA;
                                    end
                            endcase
                        end
                    end
                I2C_TX_REG_DATA: // sets reg data to write once reg address is done transmitting
                    begin
                        // LED = 8'b1111_1101;
                        if (i2c_busy == 1'b1)
                        begin
                            ena = 1'b1; // hold enable
                            rw = 1'b0; // true for all but last case (reading mag data)
                            next_state = WAIT_I2C_REG_ADDR_DONE;
                            case (count_data)
                                3'd0:
                                    wdata = 8'hE0; // 1110_0000: enable interrupts on x, y, and z magnetic data
                                3'd1:
                                    wdata = 8'h60; // 0110_0000: Acceleration data rate = 100Hz, Acceleration x, y, and z axis disabled.
                                3'd2:
                                    wdata = 8'h74; // 0111_0100: Temp Sensor =Disabled, Mag. Resolution =HIGH, Mag. data rate =100Hz
                                3'd3:
                                    wdata = 8'b0010_0000; // 0xx0_0000: xx=Magnetic Full Scale: 11=+- 12 Gauss, 10=+-8,01=+-4,00=+-2
                                3'd4:
                                    wdata = 8'h00; // 0000_0000: Magnetic Sensor Mode = Continuous-Conversion Mode
                                3'd5:
                                    begin
                                        rw = 1'b1;
                                        wdata = 8'h00; // Don't care.
                                    end
                            endcase
                        end
                    end
                WAIT_I2C_REG_ADDR_DONE:
                    begin
                        // LED = {i2c_busy, 6'b11_1011};
                        if ( i2c_busy == 1'b0 ) // done transmitting reg address and data tx has begun
                        begin
                            if (count_data == 3'd5)
                            begin
                                count_data = 3'd0;
                                next_state = WAIT_BUSY_2;
                            end else begin
                                count_data = count_data + 1;
                                next_state = WAIT_BUSY_1;
                            end
                        end
                    end
                WAIT_BUSY_1:
                    begin
                        // LED = 8'b1111_0111;
                        if ( i2c_busy == 1'b1 )
                        begin
                            ena = 1'b0; // hold enable
                            next_state = I2C_TX_REG_ADDR;
                        end
                    end
                WAIT_I2C_RX_DATA:
                    begin
                        // LED = 8'b1110_1111;
                        //test_reading = 1'b1;
                        if (i2c_busy == 1'b0)
                        begin
                            case (count_data_in)
                                // first 12 bits
                                3'd0:
                                    begin
                                        data[count_sensor] [7:0] = rdata[7:0];
                                        // data[count_sensor+1] [7:0] = right_rdata[7:0];
                                        count_data_in = count_data_in + 1;
                                        next_state = WAIT_BUSY_2;
                                    end
                                3'd1:
                                    begin
                                        //data[count_sensor] [11:8] = rdata[3:0];
                                        //data[count_sensor+1] [11:8] = right_rdata[3:0];
                                        data[count_sensor] [15:8] = rdata[7:0];
                                        // data[count_sensor+1] [15:8] = right_rdata[7:0];
                                        count_data_in = count_data_in + 1;
                                        next_state = WAIT_BUSY_2;
                                    end
                                // second 12 bits
                                3'd2:
                                    begin
                                        //data[count_sensor] [19:12] = rdata[7:0];
                                        //data[count_sensor+1] [19:12] = right_rdata[7:0];
                                        data[count_sensor] [23:16] = rdata[7:0];
                                        // data[count_sensor+1] [23:16] = right_rdata[7:0];
                                        count_data_in = count_data_in + 1;
                                        next_state = WAIT_BUSY_2;
                                    end
                                3'd3:
                                    begin
                                        //data[count_sensor] [23:20] = rdata[3:0];
                                        //data[count_sensor+1] [23:20] = right_rdata[3:0];
                                        data[count_sensor] [31:24] = rdata[7:0];
                                        // data[count_sensor+1] [31:24] = right_rdata[7:0];
                                        count_data_in = count_data_in + 1;
                                        next_state = WAIT_BUSY_2;
                                    end
                                //third 12 bits
                                3'd4:
                                    begin
                                        //data[count_sensor] [31:24] = rdata[7:0];
                                        //data[count_sensor+1] [31:24] = right_rdata[7:0];
                                        data[count_sensor] [39:32] = rdata[7:0];
                                        // data[count_sensor+1] [39:32] = right_rdata[7:0];
                                        count_data_in = count_data_in + 1;
                                        ena = 1'b0; // next byte will be last
                                        next_state = WAIT_BUSY_2;
                                    end
                                3'd5:
                                    begin
                                        //data[count_sensor] [35:32] = rdata[3:0];
                                        //data[count_sensor+1] [35:32] = right_rdata[3:0];
                                        data[count_sensor] [47:40] = rdata[7:0];
                                        // data[count_sensor+1] [47:40] = right_rdata[7:0];
                                        count_data_in = 0;
                                        
                                        // done reading 12-bit xyz data from one sensor.
                                        // repeat states 0-5 for other three sensors
                                        if (count_sensor == 1'd1)
                                        begin
                                            count_sensor = 1'd0;
                                            ready = 1'b1;
                                            next_state = IDLE; 
                                        end else begin
                                            addr = 7'b00111_10; // 01=front, 10=back
                                            count_sensor = 1'd1;
                                            next_state = I2C_TX_REG_ADDR; 
                                        end
                                    end   
                            endcase
                        end
                    end
                WAIT_BUSY_2:
                    begin
                        // LED = 8'b1101_1111;
                        if (i2c_busy == 1'b1)
                            begin
                                next_state = WAIT_I2C_RX_DATA;
                            end
                    end
            endcase
		end
	end

endmodule

