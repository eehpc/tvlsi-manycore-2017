///////////////////////////////////////////////////////////////////////////////////////////////////
// Company: <Name>
//
// File: i2c_sm.v
// File history:
//      <Revision number>: <Date>: <Comments>
//      <Revision number>: <Date>: <Comments>
//      <Revision number>: <Date>: <Comments>
//
// Description: 
//
// <Description here>
//
// Targeted device: <Family::IGLOO> <Die::AGLN250V2> <Package::100 VQFP>
// Author: <Name>
//
/////////////////////////////////////////////////////////////////////////////////////////////////// 

module i2c_sm_test_replica(
	input sys_clk,
	input sys_reset,
	input start,
    input left_active,
	output reg ready,
	output reg [47:0] front_data,
	output reg [47:0] back_data,
	inout sda_l,
	inout scl_l,
	inout sda_r,
	inout scl_r);

	wire i2c_busy;
	wire [7:0] rdata;
	wire ack_error;
	wire sys_reset_n;
	assign sys_reset_n = sys_reset;
    reg [47:0] data [1:0];
	reg ena, rw;
	reg [6:0] addr;
	reg [7:0] wdata;
	reg count_sensor ;
	reg [2:0] count_data_in, count_data ;
    reg [3:0] current_state, next_state;
	
    parameter [3:0] IDLE=			 	4'd00, 
					I2C_TX_REG_ADDR=		4'd01,
					I2C_TX_REG_DATA= 	4'd02,
					WAIT_I2C_REG_ADDR_DONE= 		4'd03,
					WAIT_BUSY_1= 		4'd04,
					WAIT_I2C_RX_DATA= 		4'd05,
					WAIT_BUSY_2= 		4'd06;

    //assign front_data = data[0];
   // assign back_data = data[1];
    integer count, in_file;
    integer err, LF_in_val, LB_in_val, RF_in_val, RB_in_val;
    reg [15:0] LF_in_val_bin, LB_in_val_bin;
    reg [47:0] RF_in_val_bin, RB_in_val_bin;

initial begin
//    in_file = $fopen({"C:/Users/Fred/Documents/MATLAB/Research/fpga_data/7 position data/with magnet-stationary/verilog_test_data.txt"}, "r");    
    in_file = $fopen({"C:/myaltera/TDS_I2C_LR/TDS_I2C/MEM/lr_testdata.txt"}, "r");    
    $display("in_file = %d", in_file);
    
end

		
		
    always @ ( next_state)
    begin
        current_state <= next_state;
    end

	always @ (posedge sys_clk)
	begin
		if( sys_reset_n == 1'b1 )
		begin
			next_state = IDLE;
            count_sensor = 1'd0;
            count_data_in = 3'd0;
            count_data = 3'd0;
            ena = 1'b0;
            ready = 1'b1;
		end else
		begin
            case ( current_state )
                IDLE:
					begin
                        ready = 1'b1;
                        count = 127;
						if( start == 1'b1 )
						begin
                            ready = 1'b0;
							next_state = I2C_TX_REG_ADDR;
						end
					end
                I2C_TX_REG_ADDR:
                    begin
                        count = count -1;
                        if ( count == 0)
                            next_state = I2C_TX_REG_DATA;
                    end
                I2C_TX_REG_DATA: // sets reg data to write once reg address is done transmitting
                    begin
                        // read data's
                        if (left_active == 1'b1) begin
                            // LF XYZ
                            count = $fscanf(in_file, "%d", LF_in_val);
                            LF_in_val_bin = LF_in_val;
                            //$display("LF_in_val = %d, in_val_bin = %b", LF_in_val, LF_in_val_bin);
                            front_data[15:0] = LF_in_val_bin[15:0];
                            count = $fscanf(in_file, "%d", LF_in_val);
                            LF_in_val_bin = LF_in_val;
//                            $display("LF_in_val = %d, in_val_bin = %b", LF_in_val, LF_in_val_bin);
                            front_data[31:16] = LF_in_val_bin[15:0];
                            count = $fscanf(in_file, "%d", LF_in_val);
                            LF_in_val_bin = LF_in_val;
//                            $display("LF_in_val = %d, in_val_bin = %b", LF_in_val, LF_in_val_bin);
                            front_data[47:32] = LF_in_val_bin[15:0];
                            
									 // LB XYZ
                            //x
                            count = $fscanf(in_file, "%d", LB_in_val);
                            LB_in_val_bin = LB_in_val;
//                            $display("LB_in_val = %d, in_val_bin = %b", LB_in_val, LB_in_val_bin);
                            back_data[15:0] = LB_in_val_bin[15:0];
                            count = $fscanf(in_file, "%d", LB_in_val); //y
                            LB_in_val_bin = LB_in_val;
//                            $display("LB_in_val = %d, in_val_bin = %b", LB_in_val, LB_in_val_bin);
                            back_data[31:16] = LB_in_val_bin[15:0];
                            count = $fscanf(in_file, "%d", LB_in_val); //z
                            LB_in_val_bin = LB_in_val;
//                            $display("LB_in_val = %d, in_val_bin = %b", LB_in_val, LB_in_val_bin);
                            back_data[47:32] = LB_in_val_bin[15:0];

                            // RF XYZ
                            count = $fscanf(in_file, "%d", RF_in_val);
                            RF_in_val_bin[15:0] = RF_in_val;
//                            $display("RF_in_val = %d, in_val_bin = %b", RF_in_val, RF_in_val_bin);
                            count = $fscanf(in_file, "%d", RF_in_val);
                            RF_in_val_bin[31:16] = RF_in_val;
//                            $display("RF_in_val = %d, in_val_bin = %b", RF_in_val, RF_in_val_bin);
                            count = $fscanf(in_file, "%d", RF_in_val);
                            RF_in_val_bin[47:32] = RF_in_val;
//                            $display("RF_in_val = %d, in_val_bin = %b", RF_in_val, RF_in_val_bin);

                            
                            // RB XYZ
                            count = $fscanf(in_file, "%d", RB_in_val);
                            RB_in_val_bin[15:0] = RB_in_val;
//                            $display("RB_in_val = %d, in_val_bin = %b", RB_in_val, RB_in_val_bin);
                            count = $fscanf(in_file, "%d", RB_in_val);
                            RB_in_val_bin[31:16] = RB_in_val;
//                            $display("RB_in_val = %d, in_val_bin = %b", RB_in_val, RB_in_val_bin);
                            count = $fscanf(in_file, "%d", RB_in_val);
                            RB_in_val_bin[47:32] = RB_in_val;
//                            $display("RB_in_val = %d, in_val_bin = %b", RB_in_val, RB_in_val_bin);
                        end else begin
                            front_data[47:32] = RF_in_val_bin[47:32];
                            front_data[31:16] = RF_in_val_bin[31:16];
                            front_data[15:0] = RF_in_val_bin[15:0];

                            back_data[47:32] = RB_in_val_bin[47:32];
                            back_data[31:16] = RB_in_val_bin[31:16];
                            back_data[15:0] = RB_in_val_bin[15:0];
                        end
                        next_state = WAIT_I2C_REG_ADDR_DONE;
                    end
                WAIT_I2C_REG_ADDR_DONE:
                    begin
                        next_state = IDLE;
                    end

            endcase
		end
	end

endmodule

