//////////////////////////////////////////////////////////////////////
////                                                              ////
//// registerInterface.v                                          ////
////                                                              ////
//// This file is part of the i2cSlave opencores effort.
//// <http://www.opencores.org/cores//>                           ////
////                                                              ////
//// Module Description:                                          ////
//// You will need to modify this file to implement your 
//// interface.
//// Add your control and status bytes/bits to module inputs and outputs,
//// and also to the I2C read and write process blocks  
////                                                              ////
//// To Do:                                                       ////
//// 
////                                                              ////
//// Author(s):                                                   ////
//// - Steve Fielding, sfielding@base2designs.com                 ////
////                                                              ////
//////////////////////////////////////////////////////////////////////
////                                                              ////
//// Copyright (C) 2008 Steve Fielding and OPENCORES.ORG          ////
////                                                              ////
//// This source file may be used and distributed without         ////
//// restriction provided that this copyright statement is not    ////
//// removed from the file and that any derivative work contains  ////
//// the original copyright notice and the associated disclaimer. ////
////                                                              ////
//// This source file is free software; you can redistribute it   ////
//// and/or modify it under the terms of the GNU Lesser General   ////
//// Public License as published by the Free Software Foundation; ////
//// either version 2.1 of the License, or (at your option) any   ////
//// later version.                                               ////
////                                                              ////
//// This source is distributed in the hope that it will be       ////
//// useful, but WITHOUT ANY WARRANTY; without even the implied   ////
//// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      ////
//// PURPOSE. See the GNU Lesser General Public License for more  ////
//// details.                                                     ////
////                                                              ////
//// You should have received a copy of the GNU Lesser General    ////
//// Public License along with this source; if not, download it   ////
//// from <http://www.opencores.org/lgpl.shtml>                   ////
////                                                              ////
//////////////////////////////////////////////////////////////////////
//
`include "i2cSlave_define.v"


module registerInterface (
  clk,
  addr,
  dataIn,
  writeEn,
  dataOut,
  reg0_auto_in,
  reg1_led_out,
  reg2_switch_in,
  reg3_button_in,
 // reg4_my_leds_out, //MAX10 Workshop
  led_sig_tds // for KNN decision
);
input clk;
input [7:0] addr;
input [7:0] dataIn;
input writeEn;
output [7:0] dataOut;
input [7:0] reg0_auto_in;
output [7:0] reg1_led_out;
input [7:0] reg2_switch_in;
input [7:0] reg3_button_in;
//output [7:0] reg4_my_leds_out; //MAX10 Workshop
input [2:0] led_sig_tds; // TDS KNN decision

reg [7:0] dataOut;
reg [7:0] reg_auto;
reg [7:0] reg_led;
reg [7:0] reg_switch;
reg [7:0] reg_button;
//reg [7:0] reg_my_leds; //MAX10 Workshop
//reg [7:0] myreg;
reg [2:0] reg_led_sig_tds;
// --- I2C Read
always @(posedge clk) begin
  case (addr)
    8'h00: dataOut <= reg_auto;  
    8'h01: dataOut <= reg_led;  
    8'h02: dataOut <= reg_switch;  
    8'h03: dataOut <= reg_button; 
	 8'h06: dataOut <= 8'h06;
	 8'h07: dataOut <= {{5{1'b0}},{reg_led_sig_tds[2:0]}};
    default: dataOut <= 8'h00;
  endcase
end


// --- I2C Write (or update from GPIO state)
always @(posedge clk) begin

	//only reg_led writeable by I2C
  if (writeEn == 1'b1) begin
    case (addr)
      8'h01: reg_led <= dataIn;
	//	8'h04: reg_my_leds <= reg2_switch_in;// MAX10 Workshop
	//	8'h05: myreg <= 8'h06;//mytest
    endcase
  end
  	
  	//other regs update from GPIO state every clock
	reg_auto     <= reg0_auto_in;
	reg_switch   <= reg2_switch_in;
	reg_button   <= reg3_button_in;
	reg_led_sig_tds <= led_sig_tds;
end

//assign reg1_led_out = reg_led;
//assign reg4_my_leds_out = led_sig_tds;//tds
endmodule


 
