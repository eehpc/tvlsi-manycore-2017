///////////////////////////////////////////////////////////////////////////////////////////////////
// Company: <Name>
//
// File: EMF.v
// File history:
//      <Revision number>: <Date>: <Comments>
//      <Revision number>: <Date>: <Comments>
//      <Revision number>: <Date>: <Comments>
//
// Description: 
//
// <Description here>
//
// Targeted device: <Family::IGLOO> <Die::AGLN250V2> <Package::100 VQFP>
// Author: <Name>
//
/////////////////////////////////////////////////////////////////////////////////////////////////// 

module EMF#(
    parameter DATA_IN_PRECISION = 16, // number of bits to use for each x/y/z value. Max 16
    parameter DATA_OUT_PRECISION = 16
    )(
    input clk,
    input reset_n,
    input i2c_ready,
    input [47:0] i2c_front_data, //Always receive all 48 bits from sensor, but only use DATA_IN_PRECISION*3
    input [47:0] i2c_back_data,
    input load_data, // from KNN/LR
    output reg i2c_start,
    output reg i2c_left_active,
    output reg data_out_valid,
    output reg [DATA_OUT_PRECISION-1:0] data_out);


reg [4:0] clk_counter;//6
reg [3*DATA_IN_PRECISION-1:0] data [3:0] ; // number of sensors = 4

reg [1:0] counter;
reg [79:0] mem [5:0]; 
reg signed [DATA_IN_PRECISION-1:0] m_in_data;
reg signed [19:0] m_mem_data;
reg [DATA_IN_PRECISION+20:0] add_in_a; // Same size as multi. adder = add_in_b + add_in_a.
//reg [35:0] add_in_b;
reg [DATA_IN_PRECISION+22:0] add_in_b;
reg [5:0] state;
reg [2:0] indexmem;
reg [2:0] indexout, data_out_index;
reg [2:0] indexinb;//new added
reg indexin;
wire [3*DATA_IN_PRECISION-1:0] in [1:0];
wire [DATA_IN_PRECISION-1:0] inb [5:0];
reg [DATA_OUT_PRECISION-1:0] out [5:0];
wire [DATA_IN_PRECISION-1:0] td [11:0];

parameter TRAIN_SIZE = 25;
reg signed [DATA_IN_PRECISION-1:0] x1_train [TRAIN_SIZE*7-1:0]; //279=40, 209=30, 174=25
reg signed [DATA_IN_PRECISION-1:0] x2_train [TRAIN_SIZE*7-1:0];
reg signed [DATA_IN_PRECISION-1:0] y1_train [TRAIN_SIZE*7-1:0];
reg signed [DATA_IN_PRECISION-1:0] y2_train [TRAIN_SIZE*7-1:0];
reg signed [DATA_IN_PRECISION-1:0] z1_train [TRAIN_SIZE*7-1:0];
reg signed [DATA_IN_PRECISION-1:0] z2_train [TRAIN_SIZE*7-1:0];

//wire [32:0] multi; //old version, 33-bit
wire [DATA_IN_PRECISION+19:0] multi;
//wire [36:0] adder;
wire [DATA_IN_PRECISION+23:0] adder;

assign multi = m_in_data * m_mem_data ;
assign adder = {{3{add_in_a[DATA_IN_PRECISION+20]}},add_in_a} + {{1{add_in_b[DATA_IN_PRECISION+22]}},add_in_b} ;

// sensor 0's x,y and z (left front)
assign inb[0] = td[0]; //x
assign inb[1] = td[1]; //y
assign inb[2] = td[2]; //z
// sensor 1's x,y, and z (right front)
assign inb[3] = td[3]; //x
assign inb[4] = td[4]; //y
assign inb[5] = td[5]; //z

// sensor 2's x, y, and z (left back)
assign in[0][DATA_IN_PRECISION-1:0] = td[6]; //x
assign in[0][(2*DATA_IN_PRECISION)-1:DATA_IN_PRECISION] = td[7]; //y
assign in[0][(3*DATA_IN_PRECISION)-1:2*DATA_IN_PRECISION] = td[8]; //z

// sensor 3's x, y, and z (right back)
assign in[1][DATA_IN_PRECISION-1:0] = td[9]; //x
assign in[1][(2*DATA_IN_PRECISION)-1:DATA_IN_PRECISION] = td[10]; //y
assign in[1][(3*DATA_IN_PRECISION)-1:2*DATA_IN_PRECISION] = td[11]; //z

assign td[0] = data[0][DATA_IN_PRECISION-1:0]; //x LF
assign td[1] = data[0][(2*DATA_IN_PRECISION)-1:DATA_IN_PRECISION];
assign td[2] = data[0][(3*DATA_IN_PRECISION)-1:2*DATA_IN_PRECISION];
assign td[3] = data[1][DATA_IN_PRECISION-1:0]; //x RF
assign td[4] = data[1][(2*DATA_IN_PRECISION)-1:DATA_IN_PRECISION];
assign td[5] = data[1][(3*DATA_IN_PRECISION)-1:2*DATA_IN_PRECISION];
assign td[6] = data[2][DATA_IN_PRECISION-1:0]; //x LB
assign td[7] = data[2][(2*DATA_IN_PRECISION)-1:DATA_IN_PRECISION]; //y
assign td[8] = data[2][(3*DATA_IN_PRECISION)-1:2*DATA_IN_PRECISION]; //z
assign td[9] = data[3][DATA_IN_PRECISION-1:0]; //x RB
assign td[10] = data[3][(2*DATA_IN_PRECISION)-1:DATA_IN_PRECISION]; //y
assign td[11] = data[3][(3*DATA_IN_PRECISION)-1:2*DATA_IN_PRECISION]; //z

always @ (posedge clk) 
begin

// 80'hzzzz_yyyy_xxxx_aaaa
// L-x
// L-y
// L-z
// R-x
// R-y
// R-z
// These values are for the Old Gray Headset
/*
mem[0]=80'h00002ffff50010536711;
mem[1]=80'hffff100109ffff81435d;
mem[2]=80'h000ec000120003a91282;
mem[3]=80'h00000ffff900104f45c7;
mem[4]=80'hffff1000fc0000fdf9d9;
mem[5]=80'h000f60000700022c4d00;
*/
// These values are for the Orange Lab Headset
//mem[0]=80'h00003ffff9001040a860;
//mem[1]=80'hffff2000fb0000ee7f82;
//mem[2]=80'h000f30000800021af23c;
//mem[3]=80'h00004ffff700103edcc4;
//mem[4]=80'hfffe80010affff80bcc7;
//mem[5]=80'h000f90001300031be69e;
mem[0]=80'h0004affff2000fcdd66d;
mem[1]=80'hfffec000fb000011b72a;
mem[2]=80'h0008e00011ffffa4c072;
mem[3]=80'h000000000a000ef237f8;
mem[4]=80'h00004000ef00005139f6;
mem[5]=80'h000f6ffffd0000057c04;

end


always @ (posedge clk)//negedge divided_clk
begin
	if(reset_n == 1'b0)
		begin
            i2c_left_active = 1'b1;
			data_out_index = 3'd0;
			clk_counter=0 ;
			state =0 ;
		end
	else
		begin					
            case ( state )
                6'd0: //IDLE
                    begin
                        data_out_index = 3'd0;
                        i2c_start = 1'b0;
                        i2c_left_active = 1'b1;
                        state = state + 1; //WAIT_I2C_READY
                    end
                6'd1: //WAIT_I2C_READY
                    begin
                        if(i2c_ready == 1'b1)
                            begin
                                i2c_start = 1'b1;
                                state = state + 1; //WAIT_I2C_STARTED;
                            end
                    end
                6'd2: //WAIT_I2C_STARTED:
                    begin
                        i2c_start = 1'b0;
                        if(i2c_ready == 1'b0)
                            begin
                                i2c_start = 1'b0;
                                state = state + 1; //WAIT_I2C_DONE;
                            end
                    end
                6'd3: //WAIT_I2C_DONE:
                    begin
                        if(i2c_ready == 1'b1)
                            begin
                                if(i2c_left_active == 1'b1)
                                    begin
                                        data[0][DATA_IN_PRECISION-1:0]                       = i2c_front_data[DATA_IN_PRECISION-1:0];
                                        data[0][(2*DATA_IN_PRECISION)-1:DATA_IN_PRECISION]   = i2c_front_data[DATA_IN_PRECISION+15:16];
                                        data[0][(3*DATA_IN_PRECISION)-1:2*DATA_IN_PRECISION] = i2c_front_data[DATA_IN_PRECISION+31:32];
                                        data[2][DATA_IN_PRECISION-1:0]                       = i2c_back_data[DATA_IN_PRECISION-1:0];
                                        data[2][(2*DATA_IN_PRECISION)-1:DATA_IN_PRECISION]   = i2c_back_data[DATA_IN_PRECISION+15:16];
                                        data[2][(3*DATA_IN_PRECISION)-1:2*DATA_IN_PRECISION] = i2c_back_data[DATA_IN_PRECISION+31:32];
                                        i2c_left_active = 1'b0;
                                        state = 6'd1; //WAIT_I2C_READY;
                                    end else
                                    begin
                                        data[1][DATA_IN_PRECISION-1:0]                       = i2c_front_data[DATA_IN_PRECISION-1:0];
                                        data[1][(2*DATA_IN_PRECISION)-1:DATA_IN_PRECISION]   = i2c_front_data[DATA_IN_PRECISION+15:16];
                                        data[1][(3*DATA_IN_PRECISION)-1:2*DATA_IN_PRECISION] = i2c_front_data[DATA_IN_PRECISION+31:32];
                                        data[3][DATA_IN_PRECISION-1:0]                       = i2c_back_data[DATA_IN_PRECISION-1:0];
                                        data[3][(2*DATA_IN_PRECISION)-1:DATA_IN_PRECISION]   = i2c_back_data[DATA_IN_PRECISION+15:16];
                                        data[3][(3*DATA_IN_PRECISION)-1:2*DATA_IN_PRECISION] = i2c_back_data[DATA_IN_PRECISION+31:32];
                                        i2c_left_active = 1'b1;
                                        state = 6'd6; // data is ready
                                    end										
                            end
                    end	
                6'd6: // done reading data from all four sensors. reset some values
                begin
                    indexmem = 3'd0;
                    indexout = 3'd0;
                    indexin = 1'b0;
                    indexinb = 3'b000; //new added
                    counter = 2'd0;
                    state = state +1;
                
                end
                6'd7:
                    begin
                        state =state +1;
                    end 
                6'd8:
                    begin
                        add_in_b = {{(DATA_IN_PRECISION+3){mem[indexmem][19]}},mem[indexmem][19:0]}; // add_in_b = a, sign extend EMF coeff.	
                        state = state + 1;
                    end
                6'd9:
                    begin
                        // in[0] = LB, in[1] = RB
                        m_in_data = in[indexin][DATA_IN_PRECISION-1:0];//{{2{in[indexin][11]}},in[indexin][11:0]};	// load sensor 2/3's (x) data
                        m_mem_data = mem[indexmem][39:20]; // load stored EMF (x) coefficiant
                        state = state + 1;
                    end
                6'd10:
                    begin
                        add_in_a = {{multi[DATA_IN_PRECISION+19]},multi}; //multi = m_in_data * m_mem_data: add_in_a = ('sensor-x' x 'EMF-x')
                        state = state + 1;
                    end
                6'd11:
                    begin
                        add_in_b = adder; // adder = {{4{add_in_a[32]}},add_in_a} + {{1{add_in_b[35]}},add_in_b}: add_in_b = a + ('sensor-x' x 'EMF-x')
                        // in[0] = LB, in[1] = RB
                        m_in_data = in[indexin][(2*DATA_IN_PRECISION)-1:DATA_IN_PRECISION];//{{2{in[indexin][23]}},in[indexin][23:12]}; // load sensor 2/3's (y?) data
                        m_mem_data = mem[indexmem][59:40]; // load stored EMF (y?) coeff
                        state = state + 1;
                    end
                6'd12:
                    begin
                        add_in_a = {{multi[DATA_IN_PRECISION+19]},multi}; //multi = m_in_data * m_mem_data: add_in_a = ('sensor-y' x 'EMF-y')
                        state = state + 1;
                    end
                6'd13:
                    begin
                        add_in_b = adder; // adder = {{4{add_in_a[32]}},add_in_a} + {{1{add_in_b[35]}},add_in_b}: add_in_b = a + ('sensor-x' x 'EMF-x') + ('sensor-y' x 'EMF-y')
                        // in[0] = LB, in[1] = RB
                        m_in_data =in[indexin][(3*DATA_IN_PRECISION)-1:2*DATA_IN_PRECISION];//{{2{ in[indexin][35]}}, in[indexin][35:24]}; // load sensor 2/3's (z) data
                        m_mem_data = mem[indexmem][79:60]; //  load stored EMF (z) coeff.
                        state = state + 1;
                    end
                6'd14:
                    begin
                        add_in_a = {{multi[DATA_IN_PRECISION+19]},multi}; //multi = m_in_data * m_mem_data: add_in_a = ('sensor-z' x 'EMF-z')
                        state = state + 1;
                    end
                6'd15:
                    begin
                        // add_in_b = a + ('sensor-x' x 'EMF-x') + ('sensor-y' x 'EMF-y') + ('sensor-z' x 'EMF-z')
                        add_in_b = adder; // adder = {{4{add_in_a[32]}},add_in_a} + {{1{add_in_b[35]}},add_in_b}
                        state = state + 1;
                    end
                6'd16:
                    begin
                        // calculate two's complement
                        add_in_a ={{(DATA_IN_PRECISION+20){1'b0}},1'b1};
                        add_in_b = ~ add_in_b;
                        state = state + 1;
                    end
                6'd17:
                    begin
                        // add_in_b = (~ add_in_b) + 33'd1 = two's complement 
                        add_in_b = adder; // adder = {{4{add_in_a[32]}},add_in_a} + {{1{add_in_b[35]}},add_in_b}
                        state = state + 1;
                    end
                6'd18:
                    begin
                        // inb[0] = LF-x, inb[1] = LF-y, LF-z, RF-x,...
                        add_in_a = {{13{inb[indexinb][DATA_IN_PRECISION-1]}},inb[indexinb],{8{1'b0}}}; // load and shift sensor 0/1's x, y, or z data
                        state = state + 1;
                    end
                6'd19:
                    begin
                        // out = (sensor0/1's x/y/z data) - (a + ('sensor2/3-x' x 'EMF-x') + ('sensor2/3-y' x 'EMF-y') + ('sensor2/3-z' x 'EMF-z'))
                        // out[0] = LF-x - LB*EMF
                        // out[1] = LF-y - LB*EMF
                        out[indexout] = adder[DATA_OUT_PRECISION+7:8]; //was [19:8]. adder = {{4{add_in_a[32]}},add_in_a} + {{1{add_in_b[35]}},add_in_b}
                        state = state + 1;
                    end
                6'd20:
                    begin
                        // out[indexout] = inb[indexinb] - in[indexin]*mem[indexmem]
                        // out[0] = LF(x) - LB*EMF_Mem[0]
                        // out[1] = LF(y) - LB*EMF_Mem[1]
                        // out[2] = LF(z) - LB*EMF_Mem[2]
                        // out[3] = RF(x) - RB*EMF_Mem[3]
                        // out[4] = RF(y) - RB*EMF_Mem[4]
                        // out[5] = RF(z) - RB*EMF_Mem[5]
                        
                        /**
                        * indexout: 0,1,2,3,4,5
                        * indexmem: 0,1,2,3,4,5
                        * counter:  0,1,2,3,4,5
                        * indexinb: 0,1,2,3,4,5
                        * indexin:  0,0,0,1,1,1
                        **/
                        if( indexout == 4'd5)
                            begin
                                state = 6'd21;
                                data_out_index = 3'd0; //set to 0, it will increment before next transfer.
                            end
                        else
                            begin
                                indexout = indexout +1;
                                indexmem = indexmem +1;
                                state = 6'd8;
                            end
                            
                        if(counter == 2'd2)
                            begin
                                indexin = ~indexin;
                                counter =2'd0;
                            end
                        else
                            counter =counter +1;
                        indexinb = indexinb + 1;
                        
                    end
                    // done calculating left/right x/y/z data and removing EMF interference.
            
            
                    6'd21 : // Begin Transmitting data to EMF/LR/etc. module
                        begin
                            if (load_data == 1'b1)
                            begin
                                data_out_valid = 1'b1; // data ready for EMF/LR/etc.
                                state = state + 1'b1;
                            end
                        end

                    6'd22 : // wait for next load signal or go to IDLE
                    begin
                        data_out_valid = 1'b0;
                        if (data_out_index == 3'd6)
                        begin
                            data_out_index = 3'd0;
                            state = 6'd0;
                        end else begin
                            data_out = out[data_out_index];
                            data_out_index = data_out_index + 1'b1;
                            state = 6'd22;
                        end
                    end
                endcase
		end
end

endmodule
